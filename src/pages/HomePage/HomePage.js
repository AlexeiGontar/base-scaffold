import React, { Component, PropTypes } from 'react';
import {Link} from 'react-router';

import Links from './components/Links';

import styles from './HomePage.scss';

class HomePage extends Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div className={styles.page}>
        <Links/>
        <Link to="/api">
          Api Page
        </Link>
      </div>
    )
  }
}

export default HomePage;

