import React, { PropTypes } from 'react'
import '../styles/core.scss'

function App({ children }) {
  return (
    <div className='page-container'>
      <div className='view-container'>
        {children}
      </div>
    </div>
  )
}

App.propTypes = {
  children: PropTypes.element
}

export default App
