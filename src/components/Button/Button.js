import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import {callApi} from './../../redux/actions/REST';
class Button extends Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div onClick={this.props.callApi}>
        Button1
      </div>
    )
  }
}
function mapStateToProps(state) {
  return {
    value: state.api
  }
}
export default connect(mapStateToProps, {
  callApi
})(Button);

