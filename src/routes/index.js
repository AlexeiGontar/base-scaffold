import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from 'containers/App'
import HomePage from 'pages/HomePage/HomePage'
import ApiPage from 'pages/ApiPage/ApiPage'

export default (store) => (
  <Route path='/' component={App}>
    <IndexRoute component={HomePage} />
    <Route path="api" component={ApiPage}/>
  </Route>
)
