import {CALL_API} from 'redux-api-middleware';
export const REQUEST = 'REQUEST';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';
function callApiAction() {
  return {
    [CALL_API]: {
      endpoint: 'http://localhost:3000/example.json',
      method: 'GET',
      types: ['REQUEST', 'SUCCESS', 'FAILURE']
    }
  }
}
export function callApi() {
  return (dispatch) => {
    dispatch(callApiAction());
  }
}
