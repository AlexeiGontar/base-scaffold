import * as ActionTypes from './../actions/REST';
const initState = 0;

export default function reducer(state= initState, action) {
  switch (action.type) {
    case ActionTypes.REQUEST: {
      return 1;
    }
    case ActionTypes.SUCCESS: {
      return 2;
    }
    case  ActionTypes.FAILURE: {
      return 3;
    }
    default: return state;
  }
}
