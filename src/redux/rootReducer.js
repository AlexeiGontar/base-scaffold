import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'
import REST from './reducers/REST'

export default combineReducers({
  REST,
  router
})
